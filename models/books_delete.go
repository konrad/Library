package models

// DeleteBookByID deletes a book by its ID
func DeleteBookByID(id int64, doer *User) error {
	// Check if the id is 0
	if id == 0 {
		return ErrIDCannotBeZero{}
	}

	// Delete the book
	_, err := x.Id(id).Delete(&Book{})

	if err != nil {
		return err
	}

	// Delete all author associations for that book
	_, err = x.Delete(&AuthorBook{BookID: id})
	if err != nil {
		return err
	}

	// Delete all quantites for this book
	_, err = x.Delete(&Quantity{ItemID: id})
	if err != nil {
		return err
	}

	// Logging
	err = logAction(ActionTypeBookDeleted, doer, id)

	return err
}
