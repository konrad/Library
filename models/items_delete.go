package models

// DeleteItemByID deletes a item by its ID
func DeleteItemByID(id int64, doer *User) error {
	// Check if the id is 0
	if id == 0 {
		return ErrIDCannotBeZero{}
	}

	// Delete the item
	_, err := x.Id(id).Delete(&Item{})

	if err != nil {
		return err
	}

	// Delete all quantites for this item
	_, err = x.Delete(&Quantity{ItemID: id})
	if err != nil {
		return err
	}

	// Logging
	err = logAction(ActionTypeItemDeleted, doer, id)

	return err
}
