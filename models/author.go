package models

// Author holds infos about an author
type Author struct {
	ID       int64  `xorm:"int(11) autoincr not null unique pk" json:"id"`
	Forename string `xorm:"varchar(250)" json:"forename"`
	Lastname string `xorm:"varchar(250) not null" json:"lastname"`
	Created  int64  `xorm:"created" json:"created"`
	Updated  int64  `xorm:"updated" json:"updated"`
}

// TableName returns the table name for struct author
func (Author) TableName() string {
	return "authors"
}

// AuthorBook holds the relation between an author and a books
type AuthorBook struct {
	ID       int64 `xorm:"int(11) autoincr not null unique pk"`
	AuthorID int64 `xorm:"int(11)"`
	BookID   int64 `xorm:"int(11)"`

	Created int64 `xorm:"created"`
	Updated int64 `xorm:"updated"`
}

// TableName returns the name for the relation author <-> book
func (AuthorBook) TableName() string {
	return "authors_books"
}

// GetAuthorByID gets information about an author by its ID
func GetAuthorByID(id int64) (author Author, exists bool, err error) {
	has, err := x.Id(id).Get(&author)

	return author, has, err
}

// GetAuthorsByBook get all authors of a book
func GetAuthorsByBook(book Book) (authors []Author, err error) {
	err = x.
		Select("authors.*").
		Table("authors_books").
		Join("INNER", "authors", "authors_books.author_id = authors.id").
		Where("book_id = ?", book.ID).
		Find(&authors)

	if err != nil {
		return authors, err
	}

	return authors, err
}
