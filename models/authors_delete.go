package models

// DeleteAuthorByID deletes an author by its ID
func DeleteAuthorByID(id int64, doer *User) error {
	// Check if the id is 0
	if id == 0 {
		return ErrIDCannotBeZero{}
	}

	// Delete the author
	_, err := x.Id(id).Delete(&Author{})

	if err != nil {
		return err
	}

	// Delete all book relations associated with that author
	_, err = x.Delete(&AuthorBook{AuthorID: id})
	if err != nil {
		return err
	}

	// Logging
	err = logAction(ActionTypeAuthorDeleted, doer, id)

	return err
}
