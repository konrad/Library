package models

// Item holds an item
type Item struct {
	ID          int64   `xorm:"int(11) autoincr not null unique pk" json:"id"`
	Title       string  `xorm:"varchar(250)" json:"title"`
	Price       float64 `xorm:"double" json:"price"`
	Description string  `xorm:"varchar(750)" json:"description"`
	Other       string  `xorm:"varchar(750)" json:"other"`
	Created     int64   `xorm:"created" json:"created"`
	Updated     int64   `xorm:"updated" json:"updated"`

	Quantity int64 `xorm:"-" json:"quantity"`
}

// TableName returns the table name for items
func (Item) TableName() string {
	return "items"
}

// GetItemByID returns an item by its ID
func GetItemByID(id int64) (item Item, exists bool, err error) {
	exists, err = x.Id(id).Get(&item)
	if err != nil {
		return
	}

	item.Quantity, err = item.getQuantity()

	return
}
