package models

// Status holds a status
type Status struct {
	ID   int64  `xorm:"int(11) autoincr not null unique pk"`
	Name string `xorm:"varchar(250)"`
}

// GetStatusList returns an array with all status
func GetStatusList() (status []Status, err error) {
	err = x.Find(&status)
	return
}

// GetStatusByID returns a status object with all its infos
func GetStatusByID(id int64) (status Status, err error) {
	_, err = x.Where("id = ?", id).Get(&status)
	return
}
