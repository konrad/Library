package models

// AddOrUpdateItem adds or updates a item from a item struct
func AddOrUpdateItem(item Item, doer *User) (newItem Item, err error) {
	// save the quantity for later use
	qty := item.Quantity

	if item.ID == 0 {
		if item.Title == "" { // Only insert it if the title is not empty
			return Item{}, ErrItemTitleCannotBeEmpty{}
		}

		_, err = x.Insert(&item)

		if err != nil {
			return Item{}, err
		}

		// Log
		err = logAction(ActionTypeItemAdded, doer, item.ID)
		if err != nil {
			return Item{}, err
		}
	} else {
		_, err = x.ID(item.ID).Update(&item)

		if err != nil {
			return Item{}, err
		}

		// Log
		err = logAction(ActionTypeItemUpdated, doer, item.ID)
		if err != nil {
			return Item{}, err
		}
	}

	// Set the Quantity
	err = item.setQuantity(qty)
	if err != nil {
		return Item{}, err
	}

	newItem, _, err = GetItemByID(item.ID)

	return newItem, err
}
