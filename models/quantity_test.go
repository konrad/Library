package models

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSetQuantity(t *testing.T) {
	// Create test database
	assert.NoError(t, PrepareTestDatabase())

	// Set Quantity for a nonexistent item (should create)
	err := SetQuantity(9999, 12)
	assert.NoError(t, err)

	// Check
	qty, err := GetQuantity(9999)
	assert.NoError(t, err)
	assert.Equal(t, int64(12), qty)

	// Update and check again
	err = SetQuantity(9999, 120)
	assert.NoError(t, err)

	// Check
	qty, err = GetQuantity(9999)
	assert.NoError(t, err)
	assert.Equal(t, int64(120), qty)
}
