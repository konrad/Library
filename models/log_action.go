package models

// ActionType is the action type
type ActionType int

// Define action types
const (
	ActionTypeUnknown   ActionType = -1
	ActionTypeBookAdded ActionType = iota
	ActionTypeBookUpdated
	ActionTypeBookDeleted
	ActionTypeAuthorAdded
	ActionTypeAuthorUpdated
	ActionTypeAuthorDeleted
	ActionTypePublisherAdded
	ActionTypePublisherUpdated
	ActionTypePublisherDeleted
	ActionTypeItemAdded
	ActionTypeItemUpdated
	ActionTypeItemDeleted
	ActionTypeUserAdded
	ActionTypeUserUpdated
	ActionTypeUserDeleted
	ActionTypeChangedUserPassword
)

// LogAction logs a user action
func logAction(actionType ActionType, user *User, itemID int64) (err error) {
	_, err = x.Insert(UserLog{Log: actionType, UserID: user.ID, ItemID: itemID})
	return
}

// GetAllLogs returns an array with all logs
func GetAllLogs() (logs []UserLog, err error) {
	err = x.OrderBy("id DESC").Find(&logs)
	if err != nil {
		return logs, err
	}

	return logs, err
}
