package models

// AddOrUpdateAuthor adds a new author based on an author struct
func AddOrUpdateAuthor(author Author, doer *User) (newAuthor Author, err error) {

	// If the ID is 0, insert the author, otherwise update it
	if author.ID == 0 {
		// Check if the author is empty, only insert it if not
		if author.Forename == "" && author.Lastname == "" {
			return Author{}, ErrAuthorCannotBeEmpty{}
		}
		_, err = x.Insert(&author)

		if err != nil {
			return Author{}, err
		}

		// Log
		err = logAction(ActionTypeAuthorAdded, doer, author.ID)
		if err != nil {
			return Author{}, err
		}
	} else {
		_, err = x.Where("id = ?", author.ID).Update(&author)

		if err != nil {
			return Author{}, err
		}

		// Log
		err = logAction(ActionTypeAuthorUpdated, doer, author.ID)
		if err != nil {
			return Author{}, err
		}
	}

	// Get the newly inserted author
	newAuthor = author

	return newAuthor, err
}
