package models

// Publisher holds publisher informations
type Publisher struct {
	ID      int64  `xorm:"int(11) autoincr not null unique pk" json:"id"`
	Name    string `xorm:"varchar(250) not null" json:"name"`
	Created int64  `xorm:"created" json:"created"`
	Updated int64  `xorm:"updated" json:"updated"`
}

// TableName returns the table name for publishers struct
func (Publisher) TableName() string {
	return "publishers"
}

// GetPublisherByID returns a publisher by its ID
func GetPublisherByID(id int64) (publisher Publisher, exists bool, err error) {
	exists, err = x.Id(id).Get(&publisher)
	return
}
