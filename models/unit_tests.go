package models

import (
	"fmt"
	"github.com/go-xorm/core"
	"github.com/go-xorm/xorm"
	"gopkg.in/testfixtures.v2"
	"os"
	"path/filepath"
	"testing"
)

// MainTest creates the test engine
func MainTest(m *testing.M, pathToRoot string) {
	var err error
	fixturesDir := filepath.Join(pathToRoot, "models", "fixtures")
	if err = createTestEngine(fixturesDir); err != nil {
		fmt.Fprintf(os.Stderr, "Error creating test engine: %v\n", err)
		os.Exit(1)
	}

	os.Exit(m.Run())
}

func createTestEngine(fixturesDir string) error {
	var err error
	x, err = xorm.NewEngine("sqlite3", "file::memory:?cache=shared")
	//x, err = xorm.NewEngine("sqlite3", "db.db")
	if err != nil {
		return err
	}
	x.SetMapper(core.GonicMapper{})

	// Sync dat shit
	x.Sync(&Book{})
	x.Sync(&User{})
	x.Sync(&Publisher{})
	x.Sync(&Author{})
	x.Sync(&AuthorBook{})
	x.Sync(&Status{})
	x.Sync(&Quantity{})
	x.Sync(&quantityRelation{})
	x.Sync(&Item{})
	x.Sync(&UserLog{})

	// Show SQL-Queries if nessecary
	if os.Getenv("UNIT_TESTS_VERBOSE") == "1" {
		x.ShowSQL(true)
	}

	return InitFixtures(&testfixtures.SQLite{}, fixturesDir)
}

// PrepareTestDatabase load test fixtures into test database
func PrepareTestDatabase() error {
	return LoadFixtures()
}
