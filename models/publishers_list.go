package models

// ListPublishers returns a list with all publishers, filtered by an optional searchstring
func ListPublishers(searchterm string) (publishers []Publisher, err error) {

	if searchterm == "" {
		err = x.Find(&publishers)
	} else {
		err = x.
			Where("name LIKE ?", "%"+searchterm+"%").
			Find(&publishers)
	}

	if err != nil {
		return []Publisher{}, err
	}

	return publishers, nil
}
