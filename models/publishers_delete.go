package models

// DeletePublisherByID deletes a publisher by its ID
func DeletePublisherByID(id int64, doer *User) error {
	// Check if the id is 0
	if id == 0 {
		return ErrIDCannotBeZero{}
	}

	// Delete the publisher
	_, err := x.Id(id).Delete(&Publisher{})
	if err != nil {
		return err
	}

	// Set all publisher to 0 on all book with this publisher
	_, err = x.Table("books").
		Where("publisher_id = ?", id).
		Update(map[string]interface{}{"publisher_id": 0})
	if err != nil {
		return err
	}

	// Logging
	err = logAction(ActionTypePublisherDeleted, doer, id)

	return err
}
