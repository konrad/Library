package models

// AddOrUpdatePublisher adds or updates a publisher from a publisher struct
func AddOrUpdatePublisher(publisher Publisher, doer *User) (newPublisher Publisher, err error) {
	if publisher.ID == 0 {
		if publisher.Name == "" { // Only insert it if the name is not empty
			return Publisher{}, ErrNoPublisherName{}
		}

		_, err = x.Insert(&publisher)
		if err != nil {
			return Publisher{}, err
		}
		// Log
		err = logAction(ActionTypePublisherAdded, doer, publisher.ID)
		if err != nil {
			return Publisher{}, err
		}

	} else {
		_, err = x.ID(publisher.ID).Update(&publisher)
		if err != nil {
			return Publisher{}, err
		}

		// Log
		err = logAction(ActionTypePublisherUpdated, doer, publisher.ID)
		if err != nil {
			return Publisher{}, err
		}
	}

	newPublisher, _, err = GetPublisherByID(publisher.ID)

	return newPublisher, err
}
