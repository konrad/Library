package models

// ListAuthors returns a list with all authors, filtered by an optional searchstring
func ListAuthors(searchterm string) (authors []Author, err error) {

	if searchterm == "" {
		err = x.Find(&authors)
	} else {
		err = x.
			Where("forename LIKE ?", "%"+searchterm+"%").
			Or("lastname LIKE ?", "%"+searchterm+"%").
			Find(&authors)
	}
	if err != nil {
		return []Author{}, err
	}

	return authors, nil
}
