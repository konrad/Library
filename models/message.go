package models

// Message is a standard message
type Message struct {
	Message string `json:"message"`
}
