package models

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetStatusList(t *testing.T) {
	// Create test database
	assert.NoError(t, PrepareTestDatabase())

	// Insert some dummy data
	_, err := x.Insert(Status{Name: "new"})
	assert.NoError(t, err)
	_, err = x.Insert(Status{Name: "used"})
	assert.NoError(t, err)
	_, err = x.Insert(Status{Name: "other"})
	assert.NoError(t, err)

	// Get a status list
	list, err := GetStatusList()
	assert.NoError(t, err)
	assert.Equal(t, "new", list[0].Name)
	assert.Equal(t, "used", list[1].Name)
	assert.Equal(t, "other", list[2].Name)

	// Get a status by its ID
	status, err := GetStatusByID(1)
	assert.NoError(t, err)
	assert.Equal(t, "new", status.Name)
}
