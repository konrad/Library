package routes

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"git.kolaente.de/konrad/Library/models"
	apiv1 "git.kolaente.de/konrad/Library/routes/api/v1"
)

// NewEcho registers a new Echo instance
func NewEcho() *echo.Echo {
	e := echo.New()

	// Logger
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "${time_rfc3339}: ${remote_ip} ${method} ${status} ${uri} ${latency_human} - ${user_agent}\n",
	}))

	// Static Content
	e.Static("/assets", "assets")

	return e
}

// RegisterRoutes registers all routes for the application
func RegisterRoutes(e *echo.Echo) {

	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			res := c.Response()
			res.Header().Set("Access-Control-Allow-Origin", "*")
			res.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
			res.Header().Set("Access-Control-Allow-Headers", "authorization,content-type")
			res.Header().Set("Access-Control-Expose-Headers", "authorization,content-type")
			return next(c)
		}
	})

	// Basic Route
	e.Static("/", "frontend/dist/")

	// Login Route
	e.POST("/login", Login)

	// API Routes
	a := e.Group("/api/v1")

	// CORS_SHIT
	a.OPTIONS("/books", SetCORSHeader)
	a.OPTIONS("/books/:id", SetCORSHeader)
	a.OPTIONS("/login", SetCORSHeader)
	a.OPTIONS("/authors", SetCORSHeader)
	a.OPTIONS("/authors/:id", SetCORSHeader)
	a.OPTIONS("/publishers", SetCORSHeader)
	a.OPTIONS("/publishers/:id", SetCORSHeader)
	a.OPTIONS("/status", SetCORSHeader)
	a.OPTIONS("/status/:id", SetCORSHeader)
	a.OPTIONS("/items", SetCORSHeader)
	a.OPTIONS("/items/:id", SetCORSHeader)
	a.OPTIONS("/logs", SetCORSHeader)
	a.OPTIONS("/users", SetCORSHeader)
	a.OPTIONS("/users/:id", SetCORSHeader)

	a.POST("/login", Login)

	// Lookup Books
	a.GET("/books", apiv1.BookList)
	a.GET("/books/:id", apiv1.BookShow)

	// Lookup Authors
	a.GET("/authors", apiv1.AuthorsList)
	a.GET("/authors/:id", apiv1.AuthorShow)

	// Lookup Publishers
	a.GET("/publishers", apiv1.PublishersList)
	a.GET("/publishers/:id", apiv1.PublisherShow)

	// Lookup Items
	a.GET("/items", apiv1.ItemsList)
	a.GET("/items/:id", apiv1.ItemShow)

	// Lookup Status
	a.GET("/status", apiv1.StatusListShow)
	a.GET("/status/:id", apiv1.StatusByIDShow)

	// ===== Routes with Authetification =====
	// Authetification
	a.Use(middleware.JWT(models.Config.JWTLoginSecret))
	a.POST("/tokenTest", apiv1.CheckToken)

	// Manage Books
	a.PUT("/books", apiv1.BookAddOrUpdate)
	a.DELETE("/books/:id", apiv1.BookDelete)
	a.POST("/books/:id", apiv1.BookAddOrUpdate)

	// Manage Authors
	a.PUT("/authors", apiv1.AuthorAddOrUpdate)
	a.DELETE("/authors/:id", apiv1.AuthorDelete)
	a.POST("/authors/:id", apiv1.AuthorAddOrUpdate)

	// Manage Publishers
	a.PUT("/publishers", apiv1.PublisherAddOrUpdate)
	a.DELETE("/publishers/:id", apiv1.PublisherDelete)
	a.POST("/publishers/:id", apiv1.PublisherAddOrUpdate)

	// Manage Items
	a.PUT("/items", apiv1.ItemAddOrUpdate)
	a.DELETE("/items/:id", apiv1.ItemDelete)
	a.POST("/items/:id", apiv1.ItemAddOrUpdate)

	// ====== Admin Routes ======

	// Manage Users
	a.GET("/users", apiv1.UsersList)
	a.PUT("/users", apiv1.UserAddOrUpdate)
	a.POST("/users/:id", apiv1.UserAddOrUpdate)
	a.GET("/users/:id", apiv1.UserShow)
	a.DELETE("/users/:id", apiv1.UserDelete)
	a.POST("/users/:id/password", apiv1.UserChangePassword)

	// View logs
	a.GET("/logs", apiv1.ShowLogs)

	/*
		Alles nur mit Api machen, davor dann einen onepager mit vue.js.

		(Alles mit | benötigt Authentifizierung)

		Routes:
		GET    / 					   - entweder übersicht anzeigen (wenn der nutzer eingeloggt ist) oder auf /login weiterleiten
		POST   /login 				   - ✔ Einloggen
		POST   /logout				   - ausloggen

		GET    /books/:id			   - ✔ Buch anzeigen
		POST   /books/:id	           - ✔ |Buch bearbeiten (inkl mengen)
		DELETE /books/:id	     	   - ✔ |Buch löschen (+alle einträge in authors_books löschen dessen Bush dazu gehört)
		GET    /books/search?s=se      - ✔ Suchen
		GET    /books/list			   - ✔ Auflisten
		PUT    /books/add			   - ✔ |Hinzufügen

		GET    /authors/:id		       - ✔ Autor anzeigen
		POST   /authors/:id       	   - ✔ |Autor bearbeiten
		DELETE /authors/:id			   - ✔ |Autor löschen (auch mit allem in books_author)
		GET    /authors/list		   - ✔ Autoren auflisten
		GET    /authors/search?s=d     - ✔ Autoren suchen
		PUT    /authors/add		  	   - ✔ |Hinzufügen

		GET    /publishers/:id		   - ✔ Verlag anzeigen
		POST   /publishers/:id		   - ✔ |Verlag bearbeiten
		DELETE /publishers/:id		   - ✔ |Verlag löschen (bei büchern Verlag auf 0 setzen)
		GET    /publishers/list	       - ✔ Verlage auflisten
		GET    /publishers/search?s=   - ✔ Verlage suchen
		PUT    /publishers/add		   - ✔ |Hinzufügen

		GET    /settings			   - |Nutzereinstellungen (Passwort, name etc)
		POST   /settings			   - |Nutzereinstellungen (Passwort, name etc)
		GET    /user 				   - |Nutzer anzeigen --> Auch nur admin
		PUT    /user 			  	   - |neue Nutzer anlegen --> Nur admin
		DELETE /user/:id 			   - |nutzer löschen --> Nur admins (sich selber löschen sollte nicht möglich sein)
		POST   /user/:id	 		   - |nutzer bearbeiten --> Sollte entweder Admin oder der Nutzer selbst sein

	*/
}
