package v1

import (
	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
	"net/http"
)

// ItemsList is the handler to list Items, optionally filtered
func ItemsList(c echo.Context) error {

	// Prepare the searchterm
	search := c.QueryParam("s")

	list, err := models.ListItems(search)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Error getting items."})
	}

	return c.JSON(http.StatusOK, list)
}
