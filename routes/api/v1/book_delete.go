package v1

import (
	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

// BookDelete is the handler to delete a book
func BookDelete(c echo.Context) error {

	id := c.Param("id")

	// Make int
	bookID, err := strconv.ParseInt(id, 10, 64)

	if err != nil {
		return c.JSON(http.StatusBadRequest, models.Message{"Book ID is invalid."})
	}

	// Check if the book exists
	_, exists, err := models.GetBookByID(bookID)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Could not get book."})
	}

	if !exists {
		return c.JSON(http.StatusNotFound, models.Message{"The book does not exist."})
	}

	// Get the user options
	doer, err := models.GetCurrentUser(c)
	if err != nil {
		return err
	}

	// Delete it
	err = models.DeleteBookByID(bookID, &doer)

	if err != nil {
		if models.IsErrIDCannotBeZero(err) {
			return c.JSON(http.StatusBadRequest, models.Message{"Id cannot be 0"})
		}
		return c.JSON(http.StatusInternalServerError, models.Message{"Could not delete book."})
	}

	return c.JSON(http.StatusOK, models.Message{"success"})
}
