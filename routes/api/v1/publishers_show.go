package v1

import (
	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

// PublisherShow is the handler to show informations about a publisher
func PublisherShow(c echo.Context) error {
	publisher := c.Param("id")

	if publisher == "" {
		return c.JSON(http.StatusBadRequest, models.Message{"Publisher ID cannot be empty."})
	}

	// Make int
	publisherID, err := strconv.ParseInt(publisher, 10, 64)
	if err != nil {
		return c.JSON(http.StatusBadRequest, models.Message{"Publisher ID is invalid."})
	}

	// Get Publisher Infos
	publisherInfos, exists, err := models.GetPublisherByID(publisherID)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Error getting publisher infos."})
	}

	// Check if it exists
	if !exists {
		return c.JSON(http.StatusNotFound, models.Message{"Publisher not found."})
	}

	return c.JSON(http.StatusOK, publisherInfos)

}
