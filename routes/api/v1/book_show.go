package v1

import (
	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

// BookShow is the handler to show informations about a book
func BookShow(c echo.Context) error {
	book := c.Param("id")

	if book == "" {
		return c.JSON(http.StatusBadRequest, models.Message{"Book id cannot be empty."})
	}

	// Make int
	bookID, err := strconv.ParseInt(book, 10, 64)
	if err != nil {
		return c.JSON(http.StatusBadRequest, models.Message{"Book ID is invalid."})
	}

	// Get book infos
	bookInfo, exists, err := models.GetBookByID(bookID)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Could not get book infos."})
	}

	// Check if it exists
	if !exists {
		return c.JSON(http.StatusNotFound, models.Message{"Book not found."})
	}

	return c.JSON(http.StatusOK, bookInfo)
}
