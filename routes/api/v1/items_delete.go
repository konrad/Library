package v1

import (
	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

// ItemDelete is the handler to delete a item
func ItemDelete(c echo.Context) error {

	id := c.Param("id")

	// Make int
	itemID, err := strconv.ParseInt(id, 10, 64)

	if err != nil {
		return c.JSON(http.StatusBadRequest, models.Message{"Item ID is invalid."})
	}

	// Check if the item exists
	_, exists, err := models.GetItemByID(itemID)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Could not get item."})
	}

	if !exists {
		return c.JSON(http.StatusBadRequest, models.Message{"The item does not exist."})
	}

	// Get the user options
	doer, err := models.GetCurrentUser(c)
	if err != nil {
		return err
	}

	// Delete it
	err = models.DeleteItemByID(itemID, &doer)

	if err != nil {
		if models.IsErrIDCannotBeZero(err) {
			return c.JSON(http.StatusBadRequest, models.Message{"Id cannot be 0"})
		}
		return c.JSON(http.StatusInternalServerError, models.Message{"Could not delete item."})
	}

	return c.JSON(http.StatusOK, models.Message{"success"})
}
