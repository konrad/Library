package v1

import (
	"net/http"

	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
)

// UsersList lists all users
func UsersList(c echo.Context) error {

	// Check if the user is admin
	if !models.IsAdmin(c) {
		return echo.ErrUnauthorized
	}

	// Prepare the searchterm
	search := c.QueryParam("s")

	list, err := models.ListUsers(search)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Error getting users."})
	}

	return c.JSON(http.StatusOK, list)
}
