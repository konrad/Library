package v1

import (
	"github.com/labstack/echo"
	"net/http"

	"fmt"
	"git.kolaente.de/konrad/Library/models"
)

// BookList is the handler to list books
func BookList(c echo.Context) error {

	// Prepare the searchterm
	search := c.QueryParam("s")

	list, err := models.ListBooks(search)

	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, models.Message{"Error getting books."})
	}

	return c.JSON(http.StatusOK, list)
}
