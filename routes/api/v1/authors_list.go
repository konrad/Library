package v1

import (
	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
	"net/http"
)

// AuthorsList is the handler to list authors
func AuthorsList(c echo.Context) error {

	// Prepare the searchterm
	search := c.QueryParam("s")

	list, err := models.ListAuthors(search)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Error getting authors."})
	}

	return c.JSON(http.StatusOK, list)
}
