package v1

import (
	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

// StatusListShow is the requesthandler to get a list with all status
func StatusListShow(c echo.Context) error {
	status, err := models.GetStatusList()

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"An error occured."})
	}

	return c.JSON(http.StatusOK, status)
}

// StatusByIDShow is the requesthandler to get a status by its ID
func StatusByIDShow(c echo.Context) error {
	statusIn := c.Param("id")

	if statusIn == "" {
		return c.JSON(http.StatusBadRequest, models.Message{"Status ID cannot be empty."})
	}

	// Make int
	statusID, err := strconv.ParseInt(statusIn, 10, 64)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Error getting status infos."})
	}

	status, err := models.GetStatusByID(statusID)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"An error occured."})
	}

	return c.JSON(http.StatusOK, status)
}
