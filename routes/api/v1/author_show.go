package v1

import (
	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

// AuthorShow is the handler to show an author
func AuthorShow(c echo.Context) error {
	author := c.Param("id")

	if author == "" {
		return c.JSON(http.StatusBadRequest, models.Message{"Author ID cannot be empty."})
	}

	// Make int
	authorID, err := strconv.ParseInt(author, 10, 64)
	if err != nil {
		return c.JSON(http.StatusBadRequest, models.Message{"Author ID is invalid."})
	}

	// Get Author Infos
	authorInfos, exists, err := models.GetAuthorByID(authorID)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Error getting author infos."})
	}

	// Check if it exists
	if !exists {
		return c.JSON(http.StatusNotFound, models.Message{"Author not found."})
	}

	return c.JSON(http.StatusOK, authorInfos)

}
