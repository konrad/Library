package v1

import (
	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

// AuthorDelete is the handler for deleting an author
func AuthorDelete(c echo.Context) error {

	id := c.Param("id")

	// Make int
	authorID, err := strconv.ParseInt(id, 10, 64)

	if err != nil {
		return c.JSON(http.StatusBadRequest, models.Message{"Author ID is invalid."})
	}

	// Check if the author exists
	_, exists, err := models.GetAuthorByID(authorID)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Could not get author."})
	}

	if !exists {
		return c.JSON(http.StatusNotFound, models.Message{"The author does not exist."})
	}

	// Get the user options
	doer, err := models.GetCurrentUser(c)
	if err != nil {
		return err
	}

	// Delete it
	err = models.DeleteAuthorByID(authorID, &doer)

	if err != nil {
		if models.IsErrIDCannotBeZero(err) {
			return c.JSON(http.StatusBadRequest, models.Message{"Id cannot be 0"})
		}
		return c.JSON(http.StatusInternalServerError, models.Message{"Could not delete author."})
	}

	return c.JSON(http.StatusOK, models.Message{"success"})
}
