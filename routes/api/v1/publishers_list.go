package v1

import (
	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
	"net/http"
)

// PublishersList is the handler to list publishers
func PublishersList(c echo.Context) error {

	// Prepare the searchterm
	search := c.QueryParam("s")

	list, err := models.ListPublishers(search)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Error getting publishers."})
	}

	return c.JSON(http.StatusOK, list)
}
