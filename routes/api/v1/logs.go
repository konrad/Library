package v1

import (
	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
	"net/http"
)

// ShowLogs handels viewing logs
func ShowLogs(c echo.Context) error {

	// Check if the user is admin
	if !models.IsAdmin(c) {
		return echo.ErrUnauthorized
	}

	// Get the logs
	logs, err := models.GetAllLogs()

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Error getting logs."})
	}

	return c.JSON(http.StatusOK, logs)
}
