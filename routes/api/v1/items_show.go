package v1

import (
	"git.kolaente.de/konrad/Library/models"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

// ItemShow is the handler to show informations about a item
func ItemShow(c echo.Context) error {
	item := c.Param("id")

	if item == "" {
		return c.JSON(http.StatusBadRequest, models.Message{"Item ID cannot be empty."})
	}

	// Make int
	itemID, err := strconv.ParseInt(item, 10, 64)
	if err != nil {
		return c.JSON(http.StatusBadRequest, models.Message{"Item ID is invalid."})
	}

	// Get item Infos
	itemInfos, exists, err := models.GetItemByID(itemID)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.Message{"Error getting item infos."})
	}

	// Check if it exists
	if !exists {
		return c.JSON(http.StatusNotFound, models.Message{"Item not found."})
	}

	return c.JSON(http.StatusOK, itemInfos)

}
