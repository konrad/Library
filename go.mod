module git.kolaente.de/konrad/Library

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1-0.20171005155431-ecdeabc65495 // indirect
	github.com/denisenkom/go-mssqldb v0.10.0 // indirect
	github.com/dgrijalva/jwt-go v3.0.1-0.20170608005149-a539ee1a749a+incompatible
	github.com/go-ini/ini v1.28.2
	github.com/go-sql-driver/mysql v1.3.1-0.20171007150158-ee359f95877b
	github.com/go-xorm/builder v0.0.0-20170519032130-c8871c857d25 // indirect
	github.com/go-xorm/core v0.5.7
	github.com/go-xorm/xorm v0.6.4-0.20170930012613-29d4a0330a00
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/labstack/echo v3.1.1-0.20170426170929-1049c9613cd3+incompatible
	github.com/labstack/gommon v0.2.2-0.20170925052817-57409ada9da0 // indirect
	github.com/lib/pq v1.10.1 // indirect
	github.com/mattn/go-colorable v0.0.10-0.20170816031813-ad5389df28cd // indirect
	github.com/mattn/go-isatty v0.0.4-0.20170925054904-a5cdd64afdee // indirect
	github.com/mattn/go-oci8 v0.1.1 // indirect
	github.com/mattn/go-sqlite3 v1.5.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/stretchr/testify v1.2.1-0.20171231124224-87b1dfb5b2fa
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v0.0.0-20170224212429-dcecefd839c4 // indirect
	github.com/ziutek/mymysql v1.5.4 // indirect
	golang.org/x/crypto v0.0.0-20190325154230-a5d413f7728c
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/testfixtures.v2 v2.4.3
	gopkg.in/yaml.v2 v2.0.0 // indirect
)
