# Library

[![Build Status](https://drone.kolaente.de/api/badges/konrad/Library/status.svg)](https://drone.kolaente.de/konrad/Library)
[![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](LICENSE)
[![Download](https://img.shields.io/badge/download-v0.2-brightgreen.svg)](https://storage.kolaente.de/minio/library-release/)

An application to manage your library, books and authors.

API Docs: https://git.kolaente.de/konrad/Library/wiki/API

Download the latest release: https://storage.kolaente.de/minio/library-release/
(`master` is up-to-date with the master branch and can contain bugs, if you want a stable version, choose a version from the list)