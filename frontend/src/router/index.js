import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Books from '@/components/Books'
import BooksAddEdit from '@/components/BooksAddEdit'
import BookOverview from '@/components/BookOverview'
import Authors from '@/components/Authors'
import AuthorsAddEdit from '@/components/AuthorsAddEdit'
import AuthorOverview from '@/components/AuthorOverview'
import Publishers from '@/components/Publishers'
import PublishersAddEdit from '@/components/PublishersAddEdit'
import PublisherOverview from '@/components/PublisherOverview'
import Items from '@/components/Items'
import ItemsOverview from '@/components/ItemOverview'
import ItemsAddEdit from '@/components/ItemsAddEdit'
import Logs from '@/components/Logs'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/books',
      name: 'books',
      component: Books
    },
    {
      path: '/books/add',
      name: 'add-book',
      component: BooksAddEdit
    },
    {
      path: '/books/:id/edit',
      name: 'book-edit',
      component: BooksAddEdit
    },
    {
      path: '/books/:id',
      name: 'book-show',
      component: BookOverview
    },
    {
      path: '/authors',
      name: 'authors',
      component: Authors
    },
    {
      path: '/authors/add',
      name: 'authors-add',
      component: AuthorsAddEdit
    },
    {
      path: '/authors/:id/edit',
      name: 'author-edit',
      component: AuthorsAddEdit
    },
    {
      path: '/authors/:id',
      name: 'author-show',
      component: AuthorOverview
    },
    {
      path: '/publishers',
      name: 'publishers',
      component: Publishers
    },
    {
      path: '/publishers/add',
      name: 'publishers-add',
      component: PublishersAddEdit
    },
    {
      path: '/publishers/:id',
      name: 'publisher-show',
      component: PublisherOverview
    },
    {
      path: '/publishers/:id/edit',
      name: 'publisher-edit',
      component: PublishersAddEdit
    },
    {
      path: '/items',
      name: 'items',
      component: Items
    },
    {
      path: '/items/add',
      name: 'item-add',
      component: ItemsAddEdit
    },
    {
      path: '/items/:id',
      name: 'item-show',
      component: ItemsOverview
    },
    {
      path: '/items/:id/edit',
      name: 'item-edit',
      component: ItemsAddEdit
    },
    {
      path: '/logs',
      name: 'view-logs',
      component: Logs
    }
  ]
})
