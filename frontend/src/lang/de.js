export default {
  general: {
    loading: 'Laden...',
    error: 'Es ist ein Fehler aufgetreten.',
    success: '',
    refresh: 'Neu laden',
    search: 'Suche...',
    searchResultCount: 'Zeige {0} Ergebnisse',
    delete: 'Löschen',
    edit: 'Bearbeiten',
    cancel: 'Abbrechen',
    approve: 'Ja!',
    action: 'Aktion',
    submit: 'Senden',
    selectOne: 'Wähle einen aus',
    selectOneOrMore: 'Wähle einen oder mehr',
    name: 'Name',
    created: 'Erstellt am {date} um {time}',
    lastEdit: 'Zuletzt bearbeitet am {date} um {time}'
  },
  login: {
    title: 'Einloggen',
    username: 'Benutzername',
    password: 'Passwort',
    btn: 'Einloggen',
    loading: 'Einloggen...',
    wrong: 'Falscher Benutzername oder Passwort.'
  },
  language: {
    en: 'English',
    de: 'Deutsch',
    fr: 'Français'
  },
  nav: {
    home: 'Home',
    books: 'Bücher',
    authors: 'Autoren',
    publishers: 'Verlage',
    items: 'Artikel',
    users: 'Benutzer',
    logs: 'Logs'
  },
  books: {
    title: 'Bücherübersicht',
    description: 'Beschreibung',
    add: 'Buch hinzufügen',
    deleteHeader: 'Buch löschen',
    deleteMsg: 'Bist du sicher dass du dieses Buch löschen willst? <b>Das kann nicht Rückgängig gemacht werden!</b>',
    deleteSuccess: 'Das Buch wurde erfolgreich gelöscht.',
    gridColumns: {
      title: 'Titel',
      isbn: 'ISBN',
      year: 'Jahr',
      price: 'Preis',
      authors: 'Autor',
      publisher: 'Verlag',
      quantity: 'Menge',
      status: 'Status'
    },
    errorNoTitle: 'Bitte gib mindestens einen Titel an.',
    updatedSuccess: 'Das Buch wurde erfolgreich geupdated!',
    insertedSuccess: 'Das Buch wurde erfolgreich erstellt!'
  },
  authors: {
    title: 'Autorenübersicht',
    newAuthor: 'Neuen Autor hinzufügen',
    deleteHeader: 'Autor löschen',
    deleteMsg: 'Bist du sicher dass du diesen Autor löschen willst? <b>Das kann nicht Rückgängig gemacht werden!</b>',
    deleteSuccess: 'Der Autor wurde erfolgreich gelöscht.',
    forename: 'Vorname',
    lastname: 'Nachname',
    errorNoName: 'Bitte gibt mindestens einen Nachnamen an.',
    updatedSuccess: 'Der Autor wurde erfolgreich geupdated!',
    insertedSuccess: 'Der Autor wurde erfolgreich erstellt!'
  },
  publishers: {
    title: 'Verlagsübersicht',
    newPublisher: 'Neuen Verlag hinzufügen',
    deleteHeader: 'Verlag löschen',
    deleteText: 'Bist du sicher dass du diesen Verlag löschen willst? <b>Das kann nicht Rückgängig gemacht werden!</b>',
    deleteSuccess: 'Der Verlag wurde erfolgreich gelöscht.',
    errorNoName: 'Bitte gib einen Namen ein.',
    updatedSuccess: 'Der Verlag wurde erfolgreich geupdated!',
    insertedSuccess: 'Der Verlag wurde erfolgreich erstellt!'
  },
  items: {
    title: 'Artikelübersicht',
    newItem: 'Neuen Artikel hinzufügen',
    description: 'Beschreibung',
    gridColumns: {
      title: 'Titel',
      price: 'Preis',
      quantity: 'Menge'
    },
    deleteHeader: 'Artikel löschen',
    deleteText: 'Bist du sicher dass du diesen Artikel löschen willst? <b>Das kann nicht Rückgängig gemacht werden!</b>',
    deleteSuccess: 'Der Artikel wurde erfolgreich gelöscht.',
    errorNoTitle: 'Bitte gib mindestens einen Titel an.',
    updatedSuccess: 'Der Artikel wurde erfolgreich geupdated!',
    insertedSuccess: 'Der Artikel wurde erfolgreich erstellt!'
  },
  logs: {
    title: 'Logs',
    gridColumns: {
      user: 'Benutzer',
      action: 'Aktion',
      itemID: 'Item',
      date: 'Datum'
    },
    logActions: [
      '',
      'Buch hinzugefügt',
      'Buch geändert',
      'Buch gelöscht',
      'Author hinzugefügt',
      'Author geändert',
      'Author gelöscht',
      'Verlag hinzugefügt',
      'Verlag geändert',
      'Verlag gelöscht',
      'Artikel hinzugefügt',
      'Artikel geändert',
      'Artikel gelöscht',
      'Benutzer hinzugefügt',
      'Benutzer geändert',
      'Benutzer gelöscht',
      'Benutzerpasswort geändert'
    ]
  }
}
