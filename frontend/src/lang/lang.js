import en from './en'
import de from './de'
import fr from './fr'

export default {
  en: en,
  de: de,
  fr: fr
}
