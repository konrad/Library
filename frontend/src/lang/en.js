export default {
  general: {
    loading: 'Loading...',
    error: 'An error occured.',
    success: 'Success',
    refresh: 'Refresh',
    search: 'Search for anything...',
    searchResultCount: 'Viewing {0} resutlts',
    delete: 'Delete',
    edit: 'Edit',
    cancel: 'Cancel',
    approve: 'Yes!',
    action: 'Action',
    submit: 'Submit',
    selectOne: 'Select one',
    selectOneOrMore: 'Select one or more',
    name: 'Name',
    created: 'Created on {date} at {time}',
    lastEdit: 'Last edit on {date} at {time}'
  },
  login: {
    title: 'Login',
    username: 'Username',
    password: 'Password',
    btn: 'Login',
    loading: 'Loggig you in...',
    wrong: 'Wrong username or password.'
  },
  language: {
    en: 'English',
    de: 'Deutsch',
    fr: 'Français'
  },
  nav: {
    home: 'Home',
    books: 'Books',
    authors: 'Authors',
    publishers: 'Publishers',
    items: 'Items',
    users: 'Users',
    logs: 'Logs'
  },
  books: {
    title: 'Books overview',
    description: 'Description',
    add: 'Add a book',
    deleteHeader: 'Delete this book',
    deleteMsg: 'Are you sure you want to delete this book? <b>This cannot be undone!</b>',
    deleteSuccess: 'The book was deleted successfully.',
    gridColumns: {
      title: 'Title',
      isbn: 'ISBN',
      year: 'Year',
      price: 'Price',
      authors: 'Authors',
      publisher: 'Publisher',
      quantity: 'Quantity',
      status: 'Status'
    },
    errorNoTitle: 'Please provide at least a title.',
    updatedSuccess: 'The book was successfully updated!',
    insertedSuccess: 'The book was successfully inserted!'
  },
  authors: {
    title: 'Authors overview',
    newAuthor: 'Add new author',
    deleteHeader: 'Delete this author',
    deleteMsg: 'Are you sure you want to delete this author? <b>This cannot be undone!</b>',
    deleteSuccess: 'The author was deleted successfully.',
    forename: 'Forename',
    lastname: 'Lastname',
    errorNoName: 'Please provide at least a lastname.',
    updatedSuccess: 'The author was successfully updated!',
    insertedSuccess: 'The author was successfully inserted!'
  },
  publishers: {
    title: 'Publishers overview',
    newPublisher: 'Add a new publisher',
    deleteHeader: 'Delete this publisher',
    deleteText: 'Are you sure you want to delete this publisher? <b>This cannot be undone!</b>',
    deleteSuccess: 'The publisher was deleted successfully.',
    errorNoName: 'Please provide a name.',
    updatedSuccess: 'The publisher was successfully updated!',
    insertedSuccess: 'The publisher was successfully inserted!'
  },
  items: {
    title: 'Items overview',
    newItem: 'Add new item',
    description: 'Description',
    gridColumns: {
      title: 'Title',
      price: 'Price',
      quantity: 'Quantity'
    },
    deleteHeader: 'Delete this item',
    deleteText: 'Are you sure you want to delete this item? <b>This cannot be undone!</b>',
    deleteSuccess: 'The item was deleted successfully.',
    errorNoTitle: 'Please provide at least a title.',
    updatedSuccess: 'The item was successfully updated!',
    insertedSuccess: 'The item was successfully inserted!'
  },
  logs: {
    title: 'Logs',
    gridColumns: {
      user: 'User',
      action: 'Action',
      itemID: 'Item',
      date: 'Date'
    },
    logActions: [
      '',
      'Book added',
      'Book updated',
      'Book deleted',
      'Author added',
      'Author updated',
      'Author deleted',
      'Publisher added',
      'Publisher updated',
      'Publisher deleted',
      'Item added',
      'Item updated',
      'Item deleted',
      'User added',
      'User updated',
      'User deleted',
      'Changed user password'
    ]
  }
}
