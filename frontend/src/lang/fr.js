export default {
  general: {
    loading: 'Chargement en cours...',
    error: 'Une erreur s\'est produite.',
    success: 'Succès',
    refresh: 'Actualiser',
    search: 'Recherche...',
    searchResultCount: '{0} résultats',
    delete: 'Supprimer',
    edit: 'Modifier',
    cancel: 'Annuler',
    approve: 'Oui!',
    action: 'Action',
    submit: 'Envoyer',
    selectOne: 'Choisissez un',
    selectOneOrMore: 'Choisissez un ou plus',
    name: 'Nom',
    created: 'Crée le {date} à {time}',
    lastEdit: 'Dernière motification le {date} à {time}'
  },
  login: {
    title: 'Se connecter',
    username: 'Nom d\'utilisateur',
    password: 'Mot de passe',
    btn: 'Se connecter',
    loading: 'Connection en cours...',
    wrong: 'Nom d\'utilisateur ou mot de passe incorrect.'
  },
  language: {
    en: 'English',
    de: 'Deutsch',
    fr: 'Français'
  },
  nav: {
    home: 'Acceuil',
    books: 'Livres',
    authors: 'Auteurs',
    publishers: 'Maisons d\'éditions',
    items: 'Articles',
    users: 'Utilisateurs',
    logs: 'Logs'
  },
  books: {
    title: 'Liste des livres',
    description: 'Description',
    add: 'Ajouter un livre',
    editBook: 'Modifier un livre',
    deleteHeader: 'Supprimer le livre',
    deleteMsg: 'Est-ce que vous êtes sur que vous voulez supprimer cette livre? <b>Cette operation est irréversible !</b>',
    deleteSuccess: 'Le livre a éte supprimè avec succès.',
    gridColumns: {
      title: 'Titre',
      isbn: 'ISBN',
      year: 'An',
      price: 'Prix',
      authors: 'Auteur',
      publisher: 'Maison d\'edition',
      quantity: 'Quantité',
      status: 'Statut'
    },
    errorNoTitle: 'Veuillez au moins saisir un titre.',
    updatedSuccess: 'Le livre a éte mit a jour avec succès !',
    insertedSuccess: 'Le livre a éte crée avec succès !'
  },
  authors: {
    title: 'Liste des auteurs',
    newAuthor: 'Ajouter un auteur',
    editAuthor: 'Modifier l\'auteur',
    deleteHeader: 'Supprimer l\'auteur',
    deleteMsg: 'Est-ce que vous êtes sur que vous voulez supprimer cet auteur? <b>Cette operation est irréversible !</b>',
    deleteSuccess: 'L\'auteur a éte supprimè avec succès.',
    forename: 'Prénom',
    lastname: 'Nom de famille',
    errorNoName: 'Veuilliez au moins saisir un nom de famille.',
    updatedSuccess: 'L\'auteur a éte mit a jour avec succès !',
    insertedSuccess: 'L\'auteur a éte crée avec succès !'
  },
  publishers: {
    title: 'Liste des maisons d\'editions',
    newPublisher: 'Ajouter une nouvelle maison d\'edition',
    deleteHeader: 'Supprimer la maison d\'edition',
    deleteText: 'Est-ce que vous êtes sur que vous voulez supprimer cette maison d\'edition? <b>Cette operation est irréversible !</b>',
    deleteSuccess: 'La maison d\'edition a éte supprimè avec succès.',
    errorNoName: 'Veuilliez saisir un nom.',
    updatedSuccess: 'La maison d\'edition a éte mit a jour avec succès !',
    insertedSuccess: 'La maison d\'edition a éte crée avec succès !'
  },
  items: {
    title: 'Liste des articles',
    newItem: 'Ajouter un article',
    description: 'Description',
    gridColumns: {
      title: 'Titre',
      price: 'Prix',
      quantity: 'Quantité'
    },
    deleteHeader: 'Supprimer l\'article',
    deleteText: 'Est-ce que vous êtes sur que vous voulez supprimer cet article? <b>Cette operation est irréversible !</b>',
    deleteSuccess: 'L\'article a éte supprimè avec succès.',
    errorNoTitle: 'Veuillez au moins saisir un titre.',
    updatedSuccess: 'L\'article a éte mit a jour avec succès !',
    insertedSuccess: 'L\'article a éte crée avec succès !'
  },
  logs: {
    title: 'Logs',
    gridColumns: {
      user: 'Utilisateur',
      action: 'Action',
      itemID: 'Item',
      date: 'Date'
    },
    logActions: [
      '',
      'Livre ajouté',
      'Livre modifié',
      'Livre supprimé',
      'Auteur ajouté',
      'Auteur modifié',
      'Auteur summprimé',
      'Maison d\'edition ajoutée',
      'Maison d\'edition modifiée',
      'Maison d\'edition supprimée',
      'Article ajouté',
      'Article modifié',
      'Article supprimée',
      'Utilisateur ajouté',
      'Utilisateur modifié',
      'Utilisateur supprimé',
      'Mot de passe changé'
    ]
  }
}

/*
 _______________________________
/ BOFH excuse #418:             \
|                               |
\ Sysadmins busy fighting SPAM. /
 -------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
 */
