// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import auth from './auth'

// Semantic UI
import '../node_modules/semantic-ui-css/semantic.min.css'

// Grid import
import Grid from './components/Grid'

// Modal import
import Modal from './components/Modal'

// Font-awesome icons import
import 'vue-awesome/icons/refresh'
import 'vue-awesome/icons/sign-out'
import 'vue-awesome/icons/trash'
import 'vue-awesome/icons/edit'
import 'vue-awesome/icons/sort-asc'
import 'vue-awesome/icons/sort-desc'
import Icon from 'vue-awesome/components/Icon'

// Paginate import
import VuePaginate from 'vue-paginate'

// Multiselect import
import Multiselect from 'vue-multiselect'

// Multilanguage import
import Multilanguage from 'vue-multilanguage'
import language from './lang/lang'

// Notifications
import Notifications from 'vue-notification'

// List Component
import List from './components/List'

// Notifications
Vue.use(Notifications)

// Multiselect globally
Vue.component('multiselect', Multiselect)

// Icons setup
Vue.component('icon', Icon)

// Check the user's auth status when the app starts
auth.checkAuth()

// Register Grid component
Vue.component('grid', Grid)

// Register Grid component
Vue.component('modal', Modal)

// List Component Setup
Vue.component('list', List)

// Paginate setup
Vue.use(VuePaginate)

// Globally set focus
Vue.directive('focus', {
  // When the bound element is inserted into the DOM...
  inserted: function (el) {
    // Focus the element
    el.focus()
  }
})

// Init Multilanguage
Vue.use(Multilanguage, language)

/*
// Check permissions on route-level
// Passed via 'meta: {perms: ['auth', 'admin']}' on routes definition
router.beforeEach((to, from, next) => {
  console.log(to)
  next()
})
*/

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
